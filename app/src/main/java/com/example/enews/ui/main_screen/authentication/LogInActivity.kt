package com.example.enews.ui.main_screen.authentication

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.enews.R
import com.example.enews.Tools
import com.example.enews.extensions.isEmailValid
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import kotlinx.android.synthetic.main.activity_log_in.*
import kotlinx.android.synthetic.main.progress_layout.*


class LogInActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }

    private fun init() {
        auth = FirebaseAuth.getInstance()

        setSupportActionBar(logInToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        loginEmailEditText.isEmailValid()
        logInNextButton.setOnClickListener(this)
        loginEmailEditText.addTextChangedListener(textWatcher)
        loginPasswordEditText.addTextChangedListener(textWatcher)
    }

    override fun onClick(v: View?) {
        if (!validateForm()) {
            return
        }
        signInUsingEmail(loginEmailEditText.toString(), loginPasswordEditText.toString())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            super.onBackPressed()
        }
        return true
    }

    private fun signInUsingEmail(email: String, password: String) {
        progressLayout.visibility = View.VISIBLE
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser
                    updateUI(user)
                } else {
                    Snackbar.make(singInContainerView, "The account doesn't exist.", Snackbar.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        progressLayout.visibility = View.GONE
        if (user != null) {
            if (user.photoUrl == null || user.photoUrl.toString().isNotEmpty()){
                val intent = Intent(this, UploadPhotoActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            } else {
                val intent = Intent(this, BottomNavigationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
        }
    }

    private fun validateForm(): Boolean {
        var valid = true
        val email = loginEmailEditText.text.toString()

        if(!Tools.emailValid(email)) {
            loginEmailInputLayout.error = "Invalid email address."
            valid = false
        } else loginEmailInputLayout.error = null

        return valid
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            val email = loginEmailEditText.text.toString().trim()
            val password = loginPasswordEditText.text.toString().trim()

            if(email.isNotEmpty() && password.isNotEmpty()){
                logInNextButton.isEnabled = true
                logInNextButton.setBackgroundResource(R.drawable.create_account_button)
            } else {
                logInNextButton.isEnabled = false
                logInNextButton.setBackgroundResource(R.drawable.disable_button_color)
            }
        }
    }
}