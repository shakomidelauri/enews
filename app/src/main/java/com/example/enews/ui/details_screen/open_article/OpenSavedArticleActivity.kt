package com.example.enews.ui.details_screen.open_article

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.viewpager.widget.ViewPager
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.activity_open_saved_articles.*
import kotlinx.android.synthetic.main.open_article_webview_layout.*

class OpenSavedArticleActivity : AppCompatActivity() {

    private lateinit var openArticlesPagerAdapter : OpenArticlesPagerAdapter
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_saved_articles)
        init()
    }

    private fun init(){
        setSupportActionBar(openSavedArticlesToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        openArticlesPagerAdapter = OpenArticlesPagerAdapter(BottomNavigationActivity.activeArticleList)
        val openSavedArticlesViewPager = findViewById<View>(R.id.openSavedArticlesViewPager) as ViewPager
        openSavedArticlesViewPager.adapter = openArticlesPagerAdapter
        val tabLayout = findViewById<View>(R.id.openSavedArticlesTabDots) as TabLayout
        tabLayout.setupWithViewPager(openSavedArticlesViewPager, true)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            super.onBackPressed()
        }
        return true
    }

    override fun onPostResume() {
        super.onPostResume()
        openSavedArticlesViewPager.currentItem = BottomNavigationActivity.selectedPosition
    }

    override fun onBackPressed() {
        if (articleWebView.canGoBack()) {
            articleWebView.goBack()
        } else {
            super.onBackPressed()
        }
    }
}