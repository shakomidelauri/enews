package com.example.enews.ui.main_screen.authentication

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_upload_photo.*
import pl.aprilapps.easyphotopicker.*


class UploadPhotoActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val PERMISSIONS_REQUEST = 76
    }

    private lateinit var easyImage: EasyImage
    private lateinit var storageRef: StorageReference
    private var downloadUri : Uri? = null
    private var file : Uri = Uri.parse("https://www.qvphysiotherapy.com/wp-content/uploads/2018/12/profile-placeholder.png")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_photo)
        init()
    }

    private fun init() {
        Glide.with(this).load("https://www.qvphysiotherapy.com/wp-content/uploads/2018/12/profile-placeholder.png").into(photoImageView)

        easyImage = EasyImage.Builder(this)
            //.setChooserTitle("Pick media")
            .setChooserType(ChooserType.CAMERA_AND_GALLERY)
            .setFolderName("EasyImage sample")
            .allowMultiple(false)
            .build()

        storageRef = FirebaseStorage.getInstance().reference

        uploadPhotoButton.setOnClickListener(this)
        saveButton.setOnClickListener(this)
        skipButton.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.uploadPhotoButton -> uploadPictureOnClick()
            R.id.skipButton -> {
                val intent = Intent(this, BottomNavigationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }
            R.id.saveButton -> {
                val userId: String = FirebaseAuth.getInstance().currentUser!!.uid

                val profilePhotoRef: StorageReference = storageRef
                    .child("profilePictures")
                    .child("$userId.jpeg")

                val uploadTask = profilePhotoRef.putFile(file)
                uploadTask.continueWithTask { task ->
                    profilePhotoRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        downloadUri = task.result

                        val user = FirebaseAuth.getInstance().currentUser
                        val profileUpdates =
                            UserProfileChangeRequest.Builder()
                                .setPhotoUri(downloadUri)
                                .build()

                        user!!.updateProfile(profileUpdates)

                        val intent = Intent(this, BottomNavigationActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                    }
                }

            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : DefaultCallback() {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {

                     file = Uri.fromFile(imageFiles[0].file)

                    Glide.with(this@UploadPhotoActivity).load(file)
                        .into(photoImageView)
                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                    //Some error handling
                    error.printStackTrace()
                }

                override fun onCanceled(source: MediaSource) {
                    //Not necessary to remove any files manually anymore
                }
            })
    }

    private fun uploadPictureOnClick() {
        if (hasReadExternalStorage() && hasWriteExternalStorage() && hasCameraPermission())
            choosePhoto()
        else
            requestPermissions()
    }

    private fun choosePhoto() {
        MaterialAlertDialogBuilder(
            ContextThemeWrapper(
                this,
                R.style.Theme_MaterialComponents_Light_Dialog_Alert
            )
        )
            .setMessage("Choose Source")
            .setNeutralButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            .setNegativeButton("Gallery") { dialog, which ->
                easyImage.openGallery(this)
                dialog.dismiss()
            }
            .setPositiveButton("Camera") { dialog, which ->
                easyImage.openCameraForImage(this)
                dialog.dismiss()
            }
            .show()

    }

    private fun requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), PERMISSIONS_REQUEST
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.size > 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    choosePhoto()
            }
        }
    }


    private fun hasReadExternalStorage() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED


    private fun hasWriteExternalStorage() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED


    private fun hasCameraPermission() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.CAMERA
    ) == PackageManager.PERMISSION_GRANTED

}