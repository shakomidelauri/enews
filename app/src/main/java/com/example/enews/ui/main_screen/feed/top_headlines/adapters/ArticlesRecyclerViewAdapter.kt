package com.example.enews.ui.main_screen.feed.top_headlines.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.enews.R
import com.example.enews.Tools
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.MoreButtonOnClick
import kotlinx.android.synthetic.main.first_article_layout.view.*
import kotlinx.android.synthetic.main.second_article_layout.view.*
import java.net.URL

class ArticlesRecyclerViewAdapter(private val articles: MutableList<ArticleModel?>, private val sources: MutableList<ArticleModel.Source?>, private val openNewsOnClick : OpenNewsOnClick, private val moreButtonOnClick: MoreButtonOnClick) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val FIRST_ITEM = 1
        const val SECOND_ITEM = 2
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == FIRST_ITEM) {
            FirstViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.first_article_layout, parent, false))
        } else {
            SecondViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.second_article_layout, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    override fun getItemViewType(position: Int) : Int {

        return if (position == 0) {
            FIRST_ITEM
        } else {
            SECOND_ITEM
        }

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FirstViewHolder) {
            holder.onBind()
        } else if (holder is SecondViewHolder) {
            holder.onBind()
        }
    }

    inner class FirstViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun onBind() {
            Glide.with(itemView.context).load(articles[adapterPosition]!!.urlToImage).into(itemView.imageView1)
            Glide.with(itemView.context).load("http://logo.clearbit.com/${URL(articles[adapterPosition]!!.url).host}").into(itemView.sourceImageView1)
            itemView.sourceTextView1.text = sources[adapterPosition]!!.name
            itemView.titleTextView1.text = articles[adapterPosition]!!.title
            itemView.dateTextView1.text = Tools.dateToTimeFormat(articles[adapterPosition]!!.publishedAt)
            itemView.setOnClickListener(this)
            itemView.moreButton1.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when(v){
                itemView ->  openNewsOnClick.onClick(adapterPosition)
                itemView.moreButton1 -> moreButtonOnClick.onClick(adapterPosition)
            }

        }
    }

    inner class SecondViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun onBind() {
            Glide.with(itemView.context).load(articles[adapterPosition]!!.urlToImage).into(itemView.imageView2)
            Glide.with(itemView.context).load("http://logo.clearbit.com/${URL(articles[adapterPosition]!!.url).host}").into(itemView.sourceImageView2)
            itemView.sourceTextView2.text = sources[adapterPosition]!!.name
            itemView.titleTextView2.text = articles[adapterPosition]!!.title
            itemView.dateTextView2.text = Tools.dateToTimeFormat(articles[adapterPosition]!!.publishedAt)
            itemView.setOnClickListener(this)
            itemView.moreButton2.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when(v) {
                itemView -> openNewsOnClick.onClick(adapterPosition)
                itemView.moreButton2 -> moreButtonOnClick.onClick(adapterPosition)
            }
        }
    }

}