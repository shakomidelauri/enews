package com.example.enews.ui.main_screen.feed.top_headlines

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.enews.App
import com.example.enews.R
import com.example.enews.network.CustomCallback
import com.example.enews.network.EndPoints.categoryList
import com.example.enews.network.LoadData
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.UnsavedArticleActionBottomDialogFragment
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.API_KEY
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticle
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSourceList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.globalSavedArticleList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.selectedPosition
import com.example.enews.ui.ui_helpers.fragment_toolbar.FragmentToolbar
import com.example.enews.ui.ui_helpers.fragment_toolbar.ToolbarManager
import com.example.enews.ui.details_screen.open_article.OpenArticleActivity
import com.example.enews.ui.main_screen.feed.top_headlines.adapters.ArticlesRecyclerViewAdapter
import com.example.enews.ui.main_screen.feed.top_headlines.adapters.CategoriesRecyclerViewAdapter
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.CategoryOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.MoreButtonOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.SavedArticleActionBottomDialogFragment
import com.example.enews.ui.ui_helpers.CenterSmoothScroller
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_top_headlines.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class TopHeadlinesFragment : Fragment() {

    private var topHeadlinesArticleList = mutableListOf<ArticleModel?>()
    private var topHeadlinesSourcesList = mutableListOf<ArticleModel.Source?>()

    private lateinit var adapterArticles: ArticlesRecyclerViewAdapter
    private lateinit var smoothScroller: CenterSmoothScroller
    private lateinit var categoryButtons: List<String>
    private lateinit var categoryAdapter: CategoriesRecyclerViewAdapter
    private var categoryLM: RecyclerView.LayoutManager? = null
    private var rememberCategory = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_top_headlines, container, false)
    }

    private fun builder(): FragmentToolbar {
        return FragmentToolbar.Builder()
            .withId(R.id.topHeadlinesToolbar)
            .build()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(builder(), view).prepareToolbar()

        categoryButtons = listOf(
            App.instance.getContext().resources.getString(R.string.us),
            App.instance.getContext().resources.getString(R.string.business),
            App.instance.getContext().resources.getString(R.string.entertainment),
            App.instance.getContext().resources.getString(R.string.health),
            App.instance.getContext().resources.getString(R.string.science),
            App.instance.getContext().resources.getString(R.string.sports),
            App.instance.getContext().resources.getString(R.string.technology)
        )
        categoriesRecyclerView.layoutManager =
            LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        categoryLM = categoriesRecyclerView.layoutManager
        smoothScroller = CenterSmoothScroller(
            categoriesRecyclerView.context
        )
        OverScrollDecoratorHelper.setUpOverScroll(
            categoriesRecyclerView,
            OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL
        )
        categoryAdapter = CategoriesRecyclerViewAdapter(categoryButtons, categoryClick)
        categoriesRecyclerView.adapter = categoryAdapter
        loadCategoryNews(categoryList[0])
    }

    override fun onResume() {
        super.onResume()
        topHeadlinesShimmerLayout.startShimmer()
    }

    override fun onPause() {
        super.onPause()
        topHeadlinesShimmerLayout.stopShimmer()
    }

    private fun parseBody(body: String) {
        try {
            val json = JSONObject(body)
            if (json.has("articles")) {
                topHeadlinesArticleList =
                    Gson().fromJson(json.getString("articles"), Array<ArticleModel>::class.java)
                        .toMutableList()
                val sourcesArray: JSONArray = json.getJSONArray("articles")

                for (i in 0 until sourcesArray.length()) {
                    val sources = sourcesArray.getJSONObject(i).getJSONObject("source")
                    val sourceObject = ArticleModel.Source()

                    if (sources.has("id"))
                        sourceObject.id = sources.getString("id")
                    if (sources.has("name"))
                        sourceObject.name = sources.getString("name")
                    topHeadlinesSourcesList.add(sourceObject)
                }
            }
        } catch (e: JSONException) {
        }
    }

    private val categoryClick = object : CategoryOnClick {
        override fun onClick(view: View?, position: Int) {
            rememberCategory = position
            smoothScroller.targetPosition = position

            categoryLM?.startSmoothScroll(smoothScroller)
            refresh()
            loadCategoryNews(categoryList[position])
        }
    }

    private val newsClick = object :
        OpenNewsOnClick {
        override fun onClick(position: Int) {
            val intent =
                Intent(activity as BottomNavigationActivity, OpenArticleActivity::class.java)
            activeArticleSource = topHeadlinesSourcesList[position]
            activeArticleList = topHeadlinesArticleList
            selectedPosition = position
            activeArticleSourceList = topHeadlinesSourcesList
            startActivity(intent)
        }
    }

    private fun loadCategoryNews(category: String) {
        if (category == "us") {
            LoadData.getUS(category, API_KEY, object : CustomCallback {
                override fun onFailure(error: String) {
                    Snackbar.make(
                        headlinesContainer,
                        error,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                override fun onSuccess(body: String) {
                    parseBody(body)
                    initAdapter()
                    topHeadlinesShimmerLayout.stopShimmer()
                    topHeadlinesShimmerLayout.visibility = View.GONE
                }
            })
        } else {
            LoadData.getCategory(categoryList[0], category, API_KEY, object : CustomCallback {
                override fun onFailure(error: String) {
                    Snackbar.make(
                        headlinesContainer,
                        error,
                        Snackbar.LENGTH_SHORT
                    ).show()
                }

                override fun onSuccess(body: String) {
                    parseBody(body)
                    initAdapter()
                    topHeadlinesShimmerLayout.stopShimmer()
                    topHeadlinesShimmerLayout.visibility = View.GONE
                }
            })
        }
    }

    private fun initAdapter() {
        adapterArticles = ArticlesRecyclerViewAdapter(
            topHeadlinesArticleList,
            topHeadlinesSourcesList,
            newsClick,
            moreButtonClick
        )
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        recyclerView.adapter = adapterArticles
        adapterArticles.notifyDataSetChanged()


        swipeRefreshLayout.setOnRefreshListener {
            swipeRefreshLayout.isRefreshing = true
            refresh()
            adapterArticles.notifyDataSetChanged()

            Handler().postDelayed({
                loadCategoryNews(categoryList[rememberCategory])
                adapterArticles.notifyDataSetChanged()
                swipeRefreshLayout.isRefreshing = false
            }, 2000)
        }
    }

    private val moreButtonClick = object : MoreButtonOnClick {
        override fun onClick(position: Int) {
            activeArticle = topHeadlinesArticleList[position]
            activeArticleSource = topHeadlinesSourcesList[position]
            selectedPosition = position

            var articleIsSaved = false
            for(article in globalSavedArticleList){
                if(article!!.title == activeArticle!!.title){
                    articleIsSaved = true
                }
            }

            if(articleIsSaved){
                val addPhotoBottomDialogFragment =
                    SavedArticleActionBottomDialogFragment.newInstance()
                addPhotoBottomDialogFragment.show(
                    childFragmentManager,
                    SavedArticleActionBottomDialogFragment.TAG
                )
            } else {
                val addPhotoBottomDialogFragment =
                    UnsavedArticleActionBottomDialogFragment.newInstance()
                addPhotoBottomDialogFragment.show(
                    childFragmentManager,
                    UnsavedArticleActionBottomDialogFragment.TAG
                )
            }
        }
    }

    private fun refresh() {
        topHeadlinesArticleList.clear()
        topHeadlinesSourcesList.clear()
    }

}