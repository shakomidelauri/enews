package com.example.enews.network

import android.util.Log.d
import com.example.enews.network.StatusCodes.HTTP_200_OK
import com.example.enews.network.StatusCodes.HTTP_201_CREATED
import com.example.enews.network.StatusCodes.HTTP_204_NO_CONTENT
import com.example.enews.network.StatusCodes.HTTP_400_BAD_REQUEST
import com.example.enews.network.StatusCodes.HTTP_401_UNAUTHORIZED
import com.example.enews.network.StatusCodes.HTTP_404_NOT_FOUND
import com.example.enews.network.StatusCodes.HTTP_500_INTERNAL_SERVER_ERROR
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory


object LoadData {

    private var retrofit = Retrofit.Builder()
        .baseUrl("https://newsapi.org/v2/")
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    private var service : ApiService = retrofit.create(ApiService::class.java)

    fun getUS(country: String, apiKey: String, callback: CustomCallback) {
        val call = service.getUS(country, apiKey)
        call.enqueue(onCallback(callback))
    }

    fun getCategory(country: String, category: String, apiKey: String, callback: CustomCallback) {
        val call = service.getCategory(country, category, apiKey)
        call.enqueue(onCallback(callback))
    }

    fun getAllFromSource(source: String, apiKey: String, callback: CustomCallback){
        val call = service.getAllFromSource(source, apiKey)
        call.enqueue(onCallback(callback))
    }

    fun getSearch(q: String, apiKey : String, callback: CustomCallback) {
        val call = service.getSearch(q, apiKey)
        call.enqueue((onCallback(callback)))
    }

    fun getSources(language: String, apiKey: String, callback: CustomCallback) {
        val call = service.getSources(language, apiKey)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback : CustomCallback) = object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            d("httpfailure", "${t.message}")
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {

            val status = response.code()

            if(status == HTTP_200_OK || status == HTTP_201_CREATED){
                callback.onSuccess(response.body().toString())

            } else if (status == HTTP_400_BAD_REQUEST) {
                try {
                    val json = JSONObject(response.errorBody()!!.string())
                    if(json.has("error")){
                        callback.onFailure(json.getString("error"))
                        return
                    }
                } catch (exception: JSONException) { }

            } else if (status == HTTP_401_UNAUTHORIZED) {
                callback.onFailure("Error: UNAUTHORIZED")
            } else if (status == HTTP_404_NOT_FOUND) {
                callback.onFailure("Error: NOT FOUND")
            } else if (status == HTTP_500_INTERNAL_SERVER_ERROR) {
                callback.onFailure("SERVER ERROR")
            } else if (status == HTTP_204_NO_CONTENT) {
                callback.onFailure("Error: NO CONTENT")
            }
        }
    }
}