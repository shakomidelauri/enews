package com.example.enews.ui.main_screen.feed.top_headlines.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.enews.App
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.CategoryOnClick
import kotlinx.android.synthetic.main.single_category_button_layout.view.*

class CategoriesRecyclerViewAdapter(private val categoryButtons: List<String>, private val categoryOnClick: CategoryOnClick):
    RecyclerView.Adapter<CategoriesRecyclerViewAdapter.ViewHolder>() {

    private var currentPosition : Int = 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        fun onBind() {
            itemView.categoryButton.text = categoryButtons[adapterPosition]
            itemView.setOnClickListener(this)

            if (currentPosition == adapterPosition) {
                itemView.categoryButton.setBackgroundResource(R.drawable.category_button_selected)
                itemView.categoryButton.setTextColor(App.instance.getContext().getColor(R.color.selectedTextColor))
            } else {
                itemView.categoryButton.setBackgroundResource(R.drawable.category_button_default)
                itemView.categoryButton.setTextColor(App.instance.getContext().getColor(R.color.defaultTextColor))
            }
        }

        override fun onClick(v: View?) {
            categoryOnClick.onClick(v, adapterPosition)
            currentPosition = adapterPosition
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.single_category_button_layout, parent, false))
    }

    override fun getItemCount() = categoryButtons.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }
}