package com.example.enews.ui.ui_helpers.bottom_sheet_menus

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.enews.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.your_library_sheet_menu_popup_layout.*
import java.lang.RuntimeException


class SavedArticleActionBottomDialogFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private var mListener: SavedItemClickListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.your_library_sheet_menu_popup_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        savedSheetMenuRemove.setOnClickListener(this)
        savedSheetMenuShare.setOnClickListener(this)
        savedSheetMenuGoToWeb.setOnClickListener(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is SavedItemClickListener) {
            context
        } else {
            throw RuntimeException(
                context.toString()
                    .toString() + " must implement ItemClickListener"
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onClick(view: View) {
        mListener!!.onSavedItemClick(view)
        dismiss()
    }

    interface SavedItemClickListener {
        fun onSavedItemClick(view: View)
    }

    companion object {
        const val TAG = "SavedArticleActionBottomDialogFragment"
        fun newInstance(): SavedArticleActionBottomDialogFragment {
            return SavedArticleActionBottomDialogFragment()
        }
    }
}