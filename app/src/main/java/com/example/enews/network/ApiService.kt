package com.example.enews.network

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("top-headlines")
    fun getUS(@Query("country") country : String?, @Query("apiKey") apiKey : String?) : Call<String>

    @GET("top-headlines")
    fun getCategory(@Query("country") country : String?, @Query("category") category : String?, @Query("apiKey") apiKey : String?) : Call<String>

    @GET("top-headlines")
    fun getAllFromSource(@Query("sources") source : String?, @Query("apiKey") apiKey : String?) : Call<String>

    @GET("everything")
    fun getSearch(@Query("q") q: String?, @Query("apiKey") apiKey : String?) : Call<String>

    @GET("sources")
    fun getSources(@Query("language") language: String?, @Query("apiKey") apiKey : String?) : Call<String>

}