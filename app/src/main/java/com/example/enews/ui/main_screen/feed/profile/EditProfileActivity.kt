package com.example.enews.ui.main_screen.feed.profile

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.example.enews.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import kotlinx.android.synthetic.main.activity_edit_profile.*
import pl.aprilapps.easyphotopicker.*

class EditProfileActivity : AppCompatActivity(), View.OnClickListener {

    companion object {
        const val PERMISSIONS_REQUEST = 24
    }

    private lateinit var easyImage: EasyImage
    private lateinit var storageRef: StorageReference
    private var downloadUri: Uri? = null
    private var file: Uri? = FirebaseAuth.getInstance().currentUser!!.photoUrl

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        init()
    }

    private fun init(){
        val user = FirebaseAuth.getInstance().currentUser
        Glide.with(this).load(user!!.photoUrl).into(editProfilePhotoImageView)
        editProfileNameEditText.setText(user.displayName)

        easyImage = EasyImage.Builder(this)
            .setChooserType(ChooserType.CAMERA_AND_GALLERY)
            .setFolderName("EasyImage sample")
            .allowMultiple(false)
            .build()

        storageRef = FirebaseStorage.getInstance().reference
        editProfilePhotoButton.setOnClickListener(this)
        editProfileSaveButton.setOnClickListener(this)
        editProfileBackButton.setOnClickListener(this)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.no_change,R.anim.slide_down)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.editProfilePhotoButton -> uploadPhotoOnClick()
            R.id.editProfileSaveButton -> {
                if (editProfileNameEditText.text.toString().isEmpty()) {
                    editProfileTextInputLayout.error = "Your name field can't be empty"
                } else {
                    saveButtonOnClick()
                }
            }
            R.id.editProfileBackButton -> {
                super.onBackPressed()
                overridePendingTransition(R.anim.no_change,R.anim.slide_down)
            }
        }
    }

    private fun uploadPhotoOnClick() {
        if (hasReadExternalStorage() && hasWriteExternalStorage() && hasCameraPermission())
            choosePhoto()
        else
            requestPermissions()
    }

    private fun saveButtonOnClick() {
        val userId: String = FirebaseAuth.getInstance().currentUser!!.uid

        val profilePhotoRef: StorageReference = storageRef
            .child("profilePictures")
            .child("$userId.jpeg")

        val uploadTask = profilePhotoRef.putFile(file!!)
        uploadTask.continueWithTask { task ->
            profilePhotoRef.downloadUrl
        }.addOnCompleteListener { task ->
            if (task.isSuccessful) {

                downloadUri = task.result

                val user = FirebaseAuth.getInstance().currentUser
                val profileUpdates =
                    UserProfileChangeRequest.Builder()
                        .setDisplayName(editProfileNameEditText.text.toString())
                        .setPhotoUri(downloadUri)
                        .build()

                user!!.updateProfile(profileUpdates)
                super.onBackPressed()
                overridePendingTransition(R.anim.no_change,R.anim.slide_down)
            }
        }
    }

    private fun choosePhoto() {
        MaterialAlertDialogBuilder(
            ContextThemeWrapper(
                this,
                R.style.Theme_MaterialComponents_Light_Dialog_Alert
            )
        )
            .setMessage("Choose Source")
            .setNeutralButton("Cancel") { dialog, which ->
                dialog.dismiss()
            }
            .setNegativeButton("Gallery") { dialog, which ->
                easyImage.openGallery(this)
                dialog.dismiss()
            }
            .setPositiveButton("Camera") { dialog, which ->
                easyImage.openCameraForImage(this)
                dialog.dismiss()
            }
            .show()

    }

    private fun requestPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), PERMISSIONS_REQUEST
            )
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSIONS_REQUEST) {
            if (grantResults.size > 2) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    choosePhoto()
            }
        }
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        easyImage.handleActivityResult(
            requestCode,
            resultCode,
            data,
            this,
            object : DefaultCallback() {
                override fun onMediaFilesPicked(
                    imageFiles: Array<MediaFile>,
                    source: MediaSource
                ) {

                    file = Uri.fromFile(imageFiles[0].file)

                    Glide.with(this@EditProfileActivity).load(file)
                        .into(editProfilePhotoImageView)
                }

                override fun onImagePickerError(
                    error: Throwable,
                    source: MediaSource
                ) {
                    error.printStackTrace()
                }

                override fun onCanceled(source: MediaSource) {}
            })
    }


    private fun hasReadExternalStorage() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED


    private fun hasWriteExternalStorage() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED


    private fun hasCameraPermission() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.CAMERA
    ) == PackageManager.PERMISSION_GRANTED


}