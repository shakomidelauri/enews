package com.example.enews.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import com.example.enews.R
import com.example.enews.Tools

fun EditText.isEmailValid() {
    addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {

            if(Tools.emailValid(p0.toString())){
                setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.baseline_check_circle_outline_24, 0)
            } else {
                setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    })
}

