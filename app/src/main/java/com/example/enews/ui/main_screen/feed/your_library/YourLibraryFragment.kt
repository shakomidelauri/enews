package com.example.enews.ui.main_screen.feed.your_library

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.transition.Fade
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.DividerItemDecoration.VERTICAL
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticle
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.databaseSavedArticles
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.selectedPosition
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.userId
import com.example.enews.ui.ui_helpers.fragment_toolbar.FragmentToolbar
import com.example.enews.ui.ui_helpers.fragment_toolbar.ToolbarManager
import com.example.enews.ui.details_screen.open_article.OpenSavedArticleActivity
import com.example.enews.ui.main_screen.feed.search.SourcesRecyclerViewAdapter
import com.example.enews.ui.details_screen.open_source_articles.OpenSourceArticlesActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleList
import com.example.enews.ui.main_screen.feed.search.SourceModel
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.CategoryOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.SavedArticleActionBottomDialogFragment
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_your_library.*
import kotlinx.android.synthetic.main.single_source_layout.view.*

class YourLibraryFragment : Fragment() {

    private lateinit var adapterSavedArticles: SavedArticlesRecyclerViewAdapter
    private lateinit var adapterSavedSources: SourcesRecyclerViewAdapter

    private var savedSourceList = mutableListOf<SourceModel?>()
    private var savedArticleList = mutableListOf<ArticleModel?>()
    private var savedArticleSourceList = mutableListOf<ArticleModel.Source?>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_your_library, container, false)
        init()
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(builder(), view).prepareToolbar()

        val fade = Fade()
        fade.excludeTarget(R.id.searchToolbar, true)
        fade.excludeTarget(android.R.id.statusBarBackground, true)
        fade.excludeTarget(android.R.id.navigationBarBackground, true)
        requireActivity().window.enterTransition = fade
        requireActivity().window.exitTransition = fade

        adapterSavedSources = SourcesRecyclerViewAdapter(savedSourceList, sourceClick)
        savedSourcesRecyclerView.layoutManager =
            LinearLayoutManager(this@YourLibraryFragment.context, RecyclerView.HORIZONTAL, false)
        savedSourcesRecyclerView.adapter = adapterSavedSources

        adapterSavedArticles = SavedArticlesRecyclerViewAdapter(
            savedArticleList,
            savedArticleSourceList,
            newsClick,
            savedArticleMoreButtonClick
        )
        savedArticlesRecyclerView.addItemDecoration(
            DividerItemDecoration(
                this@YourLibraryFragment.context,
                VERTICAL
            )
        )
        savedArticlesRecyclerView.layoutManager = LinearLayoutManager(
            this@YourLibraryFragment.context,
            LinearLayoutManager.VERTICAL,
            false
        )
        savedArticlesRecyclerView.adapter = adapterSavedArticles
    }

    private fun builder(): FragmentToolbar {
        return FragmentToolbar.Builder()
            .withId(R.id.yourLibraryToolbar)
            .build()
    }

    override fun onResume() {
        super.onResume()
        Handler().postDelayed({
            adapterSavedArticles.notifyDataSetChanged()
            adapterSavedSources.notifyDataSetChanged()
        }, 1000)
    }

    private fun init() {
        databaseSavedArticles.child(userId).child("articles")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                        if (snapshot.exists()) {
                            val article = snapshot.getValue(ArticleModel::class.java)
                            savedArticleList.add(article)
                        }
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val deletedArticle = snapshot.getValue(ArticleModel::class.java)
                        var found = false
                        var removedPosition = -1

                        for (i in savedArticleList.indices) {
                            if (savedArticleList[i]!!.title == deletedArticle!!.title) {

                                found = true
                                removedPosition = i
                            }
                        }

                        if(found){
                            savedArticleList.removeAt(removedPosition)
                            savedArticleSourceList.removeAt(removedPosition)
                            adapterSavedArticles.notifyItemRemoved(removedPosition)
                        }
                    }

                    if (savedArticleList.isEmpty()) {
                        emptyNewsPlaceholder.visibility = View.VISIBLE
                    } else {
                        emptyNewsPlaceholder.visibility = View.GONE
                    }
                }
            })

        databaseSavedArticles.child(userId).child("articleSources").addChildEventListener(object : ChildEventListener {
            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                if(snapshot.exists()){
                    val source = snapshot.getValue(ArticleModel.Source::class.java)
                    d("AddedSource", source!!.name)
                    savedArticleSourceList.add(source)
                    adapterSavedArticles.notifyDataSetChanged()

                    if(savedArticleList.isEmpty()){
                        emptyNewsPlaceholder.visibility = View.VISIBLE
                    } else {
                        emptyNewsPlaceholder.visibility = View.GONE
                    }
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {}
        })

        databaseSavedArticles.child(userId).child("sources")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    if (snapshot.exists()) {
                        val source = snapshot.getValue(SourceModel::class.java)
                        savedSourceList.add(source)
                    }

                    if (savedSourceList.isEmpty()) {
                        emptySourcesPlaceholder.visibility = View.VISIBLE
                    } else {
                        emptySourcesPlaceholder.visibility = View.GONE
                    }
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        var found = false
                        var removedPosition = -1

                        for (i in savedSourceList.indices) {
                            if (savedSourceList[i]!!.name == activeSource!!.name) {
                                found = true
                                removedPosition = i
                            }
                        }

                        if(found){
                            savedSourceList.removeAt(removedPosition)
                            adapterSavedSources.notifyItemRemoved(removedPosition)
                        }

                        if (savedSourceList.isEmpty()) {
                            emptySourcesPlaceholder.visibility = View.VISIBLE
                        } else {
                            emptySourcesPlaceholder.visibility = View.GONE
                        }
                    }
                }
            })
    }

    private val newsClick = object :
        OpenNewsOnClick {
        override fun onClick(position: Int) {
            val intent =
                Intent(activity as BottomNavigationActivity, OpenSavedArticleActivity::class.java)
            selectedPosition = position
            activeArticleList = savedArticleList
            startActivity(intent)
        }
    }

    private val savedArticleMoreButtonClick = object :
        SavedArticleMoreButtonOnClick {
        override fun onClick(position: Int) {
            selectedPosition = position
            activeArticle = savedArticleList[position]
            activeArticleSource = savedArticleSourceList[position]

            val addPhotoBottomDialogFragment =
                SavedArticleActionBottomDialogFragment.newInstance()
            addPhotoBottomDialogFragment.show(
                childFragmentManager,
                SavedArticleActionBottomDialogFragment.TAG
            )
        }
    }

    private val sourceClick = object : CategoryOnClick {
        override fun onClick(view: View?, position: Int) {
            activeSource = savedSourceList[position]
            selectedPosition = position

            val intent = Intent(
                (activity as BottomNavigationActivity),
                OpenSourceArticlesActivity::class.java
            )
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                requireActivity(),
                savedSourcesRecyclerView.findViewHolderForAdapterPosition(position)!!.itemView.singleSourceImageView,
                ViewCompat.getTransitionName(
                    savedSourcesRecyclerView.findViewHolderForAdapterPosition(
                        position
                    )!!.itemView.singleSourceImageView
                )!!
            )
            startActivity(intent, options.toBundle())
        }
    }
}