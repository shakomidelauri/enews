package com.example.enews.ui.main_screen.feed.top_headlines.interfaces

interface MoreButtonOnClick {
    fun onClick(position: Int)
}