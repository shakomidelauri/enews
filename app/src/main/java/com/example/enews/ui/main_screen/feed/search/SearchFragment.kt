package com.example.enews.ui.main_screen.feed.search

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.transition.Fade
import android.view.*
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.enews.R
import com.example.enews.network.CustomCallback
import com.example.enews.network.LoadData
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.UnsavedArticleActionBottomDialogFragment
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.API_KEY
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticle
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSourceList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeSourceList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.globalSavedArticleList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.selectedPosition
import com.example.enews.ui.ui_helpers.fragment_toolbar.FragmentToolbar
import com.example.enews.ui.ui_helpers.fragment_toolbar.ToolbarManager
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.SavedArticleActionBottomDialogFragment
import com.example.enews.ui.details_screen.open_article.OpenArticleActivity
import com.example.enews.ui.details_screen.open_source_articles.OpenSourceArticlesActivity
import com.example.enews.ui.main_screen.feed.top_headlines.adapters.ArticlesRecyclerViewAdapter
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.CategoryOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.MoreButtonOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.single_source_layout.view.*
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject


class SearchFragment : Fragment() {

    private var searchArticleList = mutableListOf<ArticleModel?>()
    private var sourcesList = mutableListOf<SourceModel?>()
    private var searchSourcesList = mutableListOf<ArticleModel.Source?>()
    private lateinit var adapterArticles: ArticlesRecyclerViewAdapter
    private lateinit var adapterSources: SourcesRecyclerViewAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_search, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(builder(), view).prepareToolbar()

        val fade = Fade()
        fade.excludeTarget(R.id.searchToolbar, true)
        fade.excludeTarget(android.R.id.statusBarBackground, true)
        fade.excludeTarget(android.R.id.navigationBarBackground, true)
        requireActivity().window.enterTransition = fade
        requireActivity().window.exitTransition = fade

        LoadData.getSources("en", API_KEY, object : CustomCallback {
            override fun onFailure(error: String) {
                Snackbar.make(searchContainer, error, Snackbar.LENGTH_SHORT).show()
            }

            override fun onSuccess(body: String) {
                val json = JSONObject(body)

                if (json.has("sources")) {
                    sourcesList =
                        Gson().fromJson(json.getString("sources"), Array<SourceModel>::class.java)
                            .toMutableList()
                    activeSourceList = sourcesList
                }

                adapterSources = SourcesRecyclerViewAdapter(activeSourceList, sourceClick)
                sourcesRecyclerView.layoutManager =
                    LinearLayoutManager(this@SearchFragment.context, RecyclerView.HORIZONTAL, false)
                OverScrollDecoratorHelper.setUpOverScroll(
                    sourcesRecyclerView,
                    OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL
                )
                sourcesRecyclerView.adapter = adapterSources
            }
        })

        searchEditText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val newText = s.toString().trim()
                if (this@SearchFragment::adapterSources.isInitialized) {
                    adapterSources.filter.filter(newText)

                    if (adapterSources.itemCount == 0) {
                        searchSourcesContainer.visibility = View.GONE
                    } else {
                        searchSourcesContainer.visibility = View.VISIBLE
                    }
                }
                if (newText.length > 3) {
                    LoadData.getSearch(newText, API_KEY, object : CustomCallback {
                        override fun onFailure(error: String) {
                            Snackbar.make(searchContainer, error, Snackbar.LENGTH_SHORT).show()
                        }

                        override fun onSuccess(body: String) {
                            foundStoriesHeader.visibility = View.VISIBLE
                            searchRecyclerView.visibility = View.VISIBLE
                            parseBody(body)
                            adapterArticles = ArticlesRecyclerViewAdapter(
                                searchArticleList,
                                searchSourcesList,
                                newsClick,
                                moreButtonClick
                            )
                            searchRecyclerView.layoutManager =
                                LinearLayoutManager(this@SearchFragment.context)
                            searchRecyclerView.adapter = adapterArticles
                        }
                    })
                } else {
                    searchRecyclerView.visibility = View.GONE
                    foundStoriesHeader.visibility = View.GONE
                }
            }
        })
    }

    private fun builder(): FragmentToolbar {
        return FragmentToolbar.Builder()
            .withId(R.id.searchToolbar)
            .build()
    }

    private val sourceClick = object : CategoryOnClick {
        override fun onClick(view: View?, position: Int) {
            activeSource = sourcesList[position]
            selectedPosition = position
            val intent = Intent(
                (activity as BottomNavigationActivity),
                OpenSourceArticlesActivity::class.java
            )
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                requireActivity(),
                sourcesRecyclerView.findViewHolderForAdapterPosition(position)!!.itemView.singleSourceImageView,
                ViewCompat.getTransitionName(
                    sourcesRecyclerView.findViewHolderForAdapterPosition(position)!!.itemView.singleSourceImageView
                )!!
            )
            startActivity(intent, options.toBundle())
        }
    }

    private val newsClick = object :
        OpenNewsOnClick {
        override fun onClick(position: Int) {
            val intent =
                Intent(activity as BottomNavigationActivity, OpenArticleActivity::class.java)
            activeArticleSource = searchSourcesList[position]
            activeArticleList = searchArticleList
            selectedPosition = position
            activeArticleSourceList = searchSourcesList
            startActivity(intent)
        }
    }

    private fun parseBody(body: String) {
        try {
            val json = JSONObject(body)
            if (json.has("articles")) {
                searchArticleList =
                    Gson().fromJson(json.getString("articles"), Array<ArticleModel>::class.java)
                        .toMutableList()
                val sourcesArray: JSONArray = json.getJSONArray("articles")

                for (i in 0 until sourcesArray.length()) {
                    val sources = sourcesArray.getJSONObject(i).getJSONObject("source")
                    val sourceObject = ArticleModel.Source()

                    if (sources.has("id"))
                        sourceObject.id = sources.getString("id")
                    if (sources.has("name"))
                        sourceObject.name = sources.getString("name")
                    searchSourcesList.add(sourceObject)
                }
            }
        } catch (e: JSONException) {
        }
    }

    private val moreButtonClick = object : MoreButtonOnClick {
        override fun onClick(position: Int) {
            activeArticle = searchArticleList[position]
            activeArticleSource = searchSourcesList[position]
            selectedPosition = position

            var articleIsSaved = false
            for (article in globalSavedArticleList) {
                if (article!!.title == activeArticle!!.title) {
                    articleIsSaved = true
                }
            }

            if (articleIsSaved) {
                val addPhotoBottomDialogFragment =
                    SavedArticleActionBottomDialogFragment.newInstance()
                addPhotoBottomDialogFragment.show(
                    childFragmentManager,
                    SavedArticleActionBottomDialogFragment.TAG
                )
            } else {
                val addPhotoBottomDialogFragment =
                    UnsavedArticleActionBottomDialogFragment.newInstance()
                addPhotoBottomDialogFragment.show(
                    childFragmentManager,
                    UnsavedArticleActionBottomDialogFragment.TAG
                )
            }

        }
    }
}