package com.example.enews.ui.details_screen.open_source_articles

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.transition.Fade
import android.util.Log.d
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.CompositePageTransformer
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.example.enews.App
import com.example.enews.R
import com.example.enews.network.CustomCallback
import com.example.enews.network.LoadData
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.UnsavedArticleActionBottomDialogFragment
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.API_KEY
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticle
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSourceList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.databaseSavedArticles
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.selectedPosition
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.userId
import com.example.enews.ui.details_screen.open_article.OpenArticleActivity
import com.example.enews.ui.main_screen.feed.search.SourceModel
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.MoreButtonOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.SavedArticleActionBottomDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_open_source_articles.*
import kotlinx.android.synthetic.main.single_source_article_layout.view.*
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.net.URL
import kotlin.math.abs


class OpenSourceArticlesActivity : AppCompatActivity(),
    UnsavedArticleActionBottomDialogFragment.ItemClickListener,
    SavedArticleActionBottomDialogFragment.SavedItemClickListener {

    private var articleList = mutableListOf<ArticleModel?>()
    private var articleSourceList = mutableListOf<ArticleModel.Source?>()
    private var isFinalized = false
    private var savedSourceList = mutableListOf<SourceModel?>()
    private var savedArticleList = mutableListOf<ArticleModel?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_source_articles)
        init()
    }

    private fun init() {
        setSupportActionBar(openSourceArticlesToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val fade = Fade()
        fade.excludeTarget(R.id.openSourceArticlesToolbar, true)
        fade.excludeTarget(android.R.id.statusBarBackground, true)
        fade.excludeTarget(android.R.id.navigationBarBackground, true)
        window.enterTransition = fade
        window.exitTransition = fade

        d("urlll", "http://logo.clearbit.com/${URL(activeSource!!.url).host}")
        Glide.with(this).load("http://logo.clearbit.com/${URL(activeSource!!.url).host}")
            .into(singleSourceIV)

        sourceNameTV.text = activeSource!!.name
        sourceDescriptionTV.text = activeSource!!.description
        LoadData.getAllFromSource(activeSource!!.id, API_KEY, object : CustomCallback {
            override fun onFailure(error: String) {
                Snackbar.make(openSourceArticlesContainer, error, Snackbar.LENGTH_SHORT).show()
            }

            override fun onSuccess(body: String) {
                try {
                    val json = JSONObject(body)
                    if (json.has("articles")) {
                        articleList =
                            Gson().fromJson(
                                json.getString("articles"),
                                Array<ArticleModel>::class.java
                            )
                                .toMutableList()

                        val sourcesArray: JSONArray = json.getJSONArray("articles")

                        for (i in 0 until sourcesArray.length()) {
                            val sources = sourcesArray.getJSONObject(i).getJSONObject("source")
                            val sourceObject = ArticleModel.Source()

                            if (sources.has("id"))
                                sourceObject.id = sources.getString("id")
                            if (sources.has("name"))
                                sourceObject.name = sources.getString("name")
                            articleSourceList.add(sourceObject)
                        }
                    }
                } catch (e: JSONException) {
                }

                sourceArticlesViewPager.adapter = ArticleSlider(
                    articleList,
                    sourceArticlesViewPager,
                    openNewsClick,
                    moreButtonClick
                )

                sourceArticlesViewPager.offscreenPageLimit = 1
                sourceArticlesViewPager.getChildAt(0).overScrollMode =
                    RecyclerView.OVER_SCROLL_NEVER

                val compositePageTransformer = CompositePageTransformer()
                compositePageTransformer.addTransformer(MarginPageTransformer(40))
                compositePageTransformer.addTransformer { page, position ->
                    val r: Float = 1 - abs(position)
                    page.scaleY = 0.85f + r * 0.15f
                }

                sourceArticlesViewPager.setPageTransformer(compositePageTransformer)
            }

        })

        databaseSavedArticles.child(userId).child("sources").addChildEventListener(object :
            ChildEventListener {
            override fun onCancelled(error: DatabaseError) {}
            override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
            override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                if (snapshot.exists()) {
                    val source = snapshot.getValue(SourceModel::class.java)
                    savedSourceList.add(source)
                }
            }

            override fun onChildRemoved(snapshot: DataSnapshot) {
                if (snapshot.exists()) {

                    var found = false
                    var removedPosition = -1

                    val removedSource = snapshot.getValue(SourceModel::class.java)
                    for (i in savedSourceList.indices) {
                        if (savedSourceList[i]!!.name == removedSource!!.name) {
                            found = true
                            removedPosition = i
                        }
                    }

                    if (found) {
                        savedSourceList.removeAt(removedPosition)
                    }
                }
            }
        })

        databaseSavedArticles.child(userId).child("articles")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    if (snapshot.exists()) {
                        val article = snapshot.getValue(ArticleModel::class.java)
                        savedArticleList.add(article)
                    }
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {

                        var found = false
                        var removedPosition = -1

                        val removedArticle = snapshot.getValue(ArticleModel::class.java)
                        for (i in savedArticleList.indices) {
                            if (savedArticleList[i]!!.title == removedArticle!!.title) {
                                found = true
                                removedPosition = i

                            }
                        }

                        if (found) {
                            savedArticleList.removeAt(removedPosition)
                        }
                    }
                }
            })
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        super.onPrepareOptionsMenu(menu)
        if (isFinalized) {
            var sourceIsSaved = false
            for (source in savedSourceList) {
                if (source!!.name == activeSource!!.name) {
                    sourceIsSaved = true
                }
            }

            if (sourceIsSaved) {
                menu!!.findItem(R.id.saveSource).isVisible = false
                menu.findItem(R.id.removeSource).isVisible = true
            } else {
                menu!!.findItem(R.id.saveSource).isVisible = true
                menu.findItem(R.id.removeSource).isVisible = false
            }
            this.invalidateOptionsMenu()
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.open_source_menu, menu)
        isFinalized = true
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
                return true
            }
            R.id.saveSource -> {
                val sourceId: String? = databaseSavedArticles.push().key
                if (sourceId != null) {
                    databaseSavedArticles
                        .child(userId)
                        .child("sources")
                        .child(sourceId)
                        .setValue(activeSource)

                    Snackbar.make(openSourceArticlesContainer, "Source saved to your library", Snackbar.LENGTH_SHORT).show()
                }
                return true
            }
            R.id.goToSourceWebsite -> {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(activeSource!!.url))
                startActivity(browserIntent)
                return true
            }
            R.id.removeSource -> {
                val sourceQuery: Query =
                    databaseSavedArticles.child(userId).child("sources").orderByChild("name")
                        .equalTo(
                            activeSource!!.name
                        )

                sourceQuery.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (sourceSnapshot in dataSnapshot.children) {
                            sourceSnapshot.ref.removeValue()
                            Snackbar.make(openSourceArticlesContainer, "Source removed from your library", Snackbar.LENGTH_SHORT).show()
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
            }
        }
        return false
    }

    private val openNewsClick = object :
        OpenNewsOnClick {
        override fun onClick(position: Int) {
            val intent = Intent(this@OpenSourceArticlesActivity, OpenArticleActivity::class.java)
            activeArticleList = articleList
            selectedPosition = position
            activeArticleSourceList = articleSourceList
            activeArticleSource = articleSourceList[position]
            startActivity(intent)
        }
    }

    private val moreButtonClick = object : MoreButtonOnClick {
        override fun onClick(position: Int) {
            activeArticle = articleList[position]
            activeArticleSource = articleSourceList[position]
            selectedPosition = position

            var articleIsSaved = false
            for (article in savedArticleList) {
                if (article!!.title == activeArticle!!.title) {
                    articleIsSaved = true
                }
            }

            if (articleIsSaved) {
                val addPhotoBottomDialogFragment =
                    SavedArticleActionBottomDialogFragment.newInstance()
                addPhotoBottomDialogFragment.show(
                    supportFragmentManager,
                    SavedArticleActionBottomDialogFragment.TAG
                )
            } else {
                val addPhotoBottomDialogFragment =
                    UnsavedArticleActionBottomDialogFragment.newInstance()
                addPhotoBottomDialogFragment.show(
                    supportFragmentManager,
                    UnsavedArticleActionBottomDialogFragment.TAG
                )
            }
        }
    }

    override fun onItemClick(view: View) {
        when (view.id) {
            R.id.sheetMenuSave -> saveArticle()
            R.id.sheetMenuShare -> shareArticle()
            R.id.sheetMenuGoToWeb -> goToSource()
        }
    }

    private fun saveArticle() {
        val articleId: String? = databaseSavedArticles.push().key
        if (articleId != null) {
            databaseSavedArticles
                .child(userId)
                .child("articles")
                .child(articleId)
                .setValue(BottomNavigationActivity.activeArticle)
            databaseSavedArticles
                .child(userId)
                .child("articleSources")
                .child(articleId)
                .setValue(activeArticleSource)

            Snackbar.make(
                openSourceArticlesContainer,
                "Article saved to your library",
                Snackbar.LENGTH_SHORT
            ).show()
        }
    }

    private fun removeArticle() {

        val articleQuery: Query =
            databaseSavedArticles.child(userId).child("articles").orderByChild("title")
                .equalTo(activeArticle!!.title)

        articleQuery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (articleSnapshot in dataSnapshot.children) {

                    databaseSavedArticles.child(userId).child("articleSources")
                        .child(articleSnapshot.ref.key!!).removeValue()

                    articleSnapshot.ref.removeValue()


                    Snackbar.make(
                        openSourceArticlesContainer,
                        "Article removed from your library",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun shareArticle() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        val sharedText =
            "${BottomNavigationActivity.activeArticle?.title}\n\n${BottomNavigationActivity.activeArticle?.description}\n\n${BottomNavigationActivity.activeArticle?.url}"
        intent.putExtra(Intent.EXTRA_TEXT, sharedText)
        startActivity(Intent.createChooser(intent, "Share news using:"))
    }

    private fun goToSource() {
        val browserIntent =
            Intent(Intent.ACTION_VIEW, Uri.parse(BottomNavigationActivity.activeArticle!!.url))
        startActivity(browserIntent)
    }

    override fun onSavedItemClick(view: View) {
        when (view.id) {
            R.id.savedSheetMenuRemove -> removeArticle()
            R.id.savedSheetMenuShare -> shareArticle()
            R.id.savedSheetMenuGoToWeb -> goToSource()
        }
    }
}