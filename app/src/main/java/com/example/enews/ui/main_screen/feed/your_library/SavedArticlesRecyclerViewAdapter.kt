package com.example.enews.ui.main_screen.feed.your_library

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.enews.R
import com.example.enews.Tools
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import kotlinx.android.synthetic.main.first_article_layout.view.*
import java.net.URL

class SavedArticlesRecyclerViewAdapter(private val articles: MutableList<ArticleModel?>, private val sources: MutableList<ArticleModel.Source?>, private val openNewsOnClick : OpenNewsOnClick, private val savedArticleMoreButtonOnClick: SavedArticleMoreButtonOnClick) : RecyclerView.Adapter<SavedArticlesRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.first_article_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun onBind() {
            Glide.with(itemView.context).load(articles[adapterPosition]!!.urlToImage).into(itemView.imageView1)
            Glide.with(itemView.context).load("http://logo.clearbit.com/${URL(articles[adapterPosition]!!.url).host}").into(itemView.sourceImageView1)
            itemView.sourceTextView1.text = sources[adapterPosition]!!.name
            itemView.titleTextView1.text = articles[adapterPosition]!!.title
            itemView.dateTextView1.text = Tools.dateToTimeFormat(articles[adapterPosition]!!.publishedAt)
            itemView.setOnClickListener(this)
            itemView.moreButton1.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when(v) {
                itemView -> openNewsOnClick.onClick(adapterPosition)
                itemView.moreButton1 -> savedArticleMoreButtonOnClick.onClick(adapterPosition)
            }
        }
    }

}