package com.example.enews.ui.main_screen.feed

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.example.enews.App
import com.example.enews.R
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.SavedArticleActionBottomDialogFragment
import com.example.enews.ui.main_screen.feed.your_library.YourLibraryFragment
import com.example.enews.ui.main_screen.feed.profile.ProfileFragment
import com.example.enews.ui.main_screen.feed.search.SearchFragment
import com.example.enews.ui.main_screen.feed.search.SourceModel
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.top_headlines.TopHeadlinesFragment
import com.example.enews.ui.ui_helpers.bottom_sheet_menus.UnsavedArticleActionBottomDialogFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_bottom_navigation.*


class BottomNavigationActivity : AppCompatActivity(), UnsavedArticleActionBottomDialogFragment.ItemClickListener,
    SavedArticleActionBottomDialogFragment.SavedItemClickListener {

    private val fragment1: Fragment = TopHeadlinesFragment()
    private val fragment2: Fragment = SearchFragment()
    private val fragment3: Fragment = YourLibraryFragment()
    private val fragment4: Fragment = ProfileFragment()
    private val fm: FragmentManager = supportFragmentManager
    private var active: Fragment = fragment1


    companion object {
        var API_KEY = App.instance.getApiKeyFromJNI()
        var activeArticle: ArticleModel? =
            ArticleModel()
        var activeArticleSource: ArticleModel.Source? = ArticleModel.Source()
        var activeArticleList: MutableList<ArticleModel?> = mutableListOf()
        var activeArticleSourceList: MutableList<ArticleModel.Source?> = mutableListOf()
        var selectedPosition = -1
        var activeSourceList: MutableList<SourceModel?> = mutableListOf()
        var activeSource: SourceModel? = SourceModel()

        var globalSavedArticleList = mutableListOf<ArticleModel?>()
        var globalSavedArticleSourceList = mutableListOf<ArticleModel.Source?>()

        var databaseSavedArticles: DatabaseReference =
            FirebaseDatabase.getInstance().getReference("users")
        var userId: String = FirebaseAuth.getInstance().currentUser!!.uid
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation)
        init()
    }

    private fun init() {
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        navView.setupWithNavController(navController)

        navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        fm.beginTransaction().add(R.id.container, fragment4, "4").hide(fragment4).commit()
        fm.beginTransaction().add(R.id.container, fragment3, "3").hide(fragment3).commit()
        fm.beginTransaction().add(R.id.container, fragment2, "2").hide(fragment2).commit()
        fm.beginTransaction().add(R.id.container, fragment1, "1").commit()

        databaseSavedArticles.child(userId).child("articles")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    if(snapshot.exists()){
                        val article = snapshot.getValue(ArticleModel::class.java)
                        globalSavedArticleList.add(article)
                    }
                }
                override fun onChildRemoved(snapshot: DataSnapshot) {
                    if(snapshot.exists()){
                        val removedArticle = snapshot.getValue(ArticleModel::class.java)

                        var found = false
                        var removedPosition = -1

                        for (i in 0 until globalSavedArticleList.size){
                            if(globalSavedArticleList[i]!!.title == removedArticle!!.title){
                                found = true
                                removedPosition = i
                            }
                        }
                        if(found){
                            globalSavedArticleList.removeAt(removedPosition)
                            globalSavedArticleSourceList.removeAt(removedPosition)
                        }
                    }
                }
            })
        databaseSavedArticles.child(userId).child("articleSources")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    if(snapshot.exists()){
                        val articleSource = snapshot.getValue(ArticleModel.Source::class.java)
                        globalSavedArticleSourceList.add(articleSource)
                    }
                }
                override fun onChildRemoved(snapshot: DataSnapshot) {}
            })
    }

    private val mOnNavigationItemSelectedListener: BottomNavigationView.OnNavigationItemSelectedListener =
        object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.navigation_headlines -> {
                        fm.beginTransaction().hide(active).show(fragment1).commit()
                        active = fragment1
                        return true
                    }
                    R.id.navigation_search -> {
                        fm.beginTransaction().hide(active).show(fragment2).commit()
                        active = fragment2
                        return true
                    }
                    R.id.navigation_library -> {
                        fm.beginTransaction().hide(active).show(fragment3).commit()
                        active = fragment3
                        return true
                    }
                    R.id.navigation_profile -> {
                        fm.beginTransaction().hide(active).show(fragment4).commit()
                        active = fragment4
                        return true
                    }
                }
                return false
            }
        }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.bottom_nav_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onItemClick(view: View) {
        when (view.id) {
            R.id.sheetMenuSave -> saveArticle()
            R.id.sheetMenuShare -> shareArticle()
            R.id.sheetMenuGoToWeb -> goToSource()
        }
    }

    override fun onSavedItemClick(view: View) {
        when (view.id) {
            R.id.savedSheetMenuRemove -> removeArticle()
            R.id.savedSheetMenuShare -> shareArticle()
            R.id.savedSheetMenuGoToWeb -> goToSource()
        }
    }

    private fun saveArticle() {
        val articleId: String? = databaseSavedArticles.push().key
        if (articleId != null) {
            databaseSavedArticles
                .child(userId)
                .child("articles")
                .child(articleId)
                .setValue(activeArticle)

            databaseSavedArticles
                .child(userId)
                .child("articleSources")
                .child(articleId)
                .setValue(activeArticleSource)
        }
        Snackbar.make(this.container, "Article saved to your library", Snackbar.LENGTH_SHORT).show()
    }

    private fun removeArticle() {
        val articleQuery: Query =
            databaseSavedArticles.child(userId).child("articles").orderByChild("title")
                .equalTo(activeArticle!!.title)

        articleQuery.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                for (articleSnapshot in dataSnapshot.children) {
                    databaseSavedArticles.child(userId).child("articleSources").child(articleSnapshot.ref.key!!).removeValue()
                    articleSnapshot.ref.removeValue()

                    Snackbar.make(
                        container,
                        "Article removed from your library",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })
    }

    private fun shareArticle() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "text/plain"
        val sharedText =
            "${activeArticle?.title}\n\n${activeArticle?.description}\n\n${activeArticle?.url}"
        intent.putExtra(Intent.EXTRA_TEXT, sharedText)
        startActivity(Intent.createChooser(intent, "Share news using:"))
    }

    private fun goToSource() {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(activeArticle!!.url))
        startActivity(browserIntent)
    }


}