package com.example.enews.ui.main_screen.authentication

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.enews.R
import com.example.enews.Tools
import com.example.enews.extensions.isEmailValid
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.UserProfileChangeRequest
import kotlinx.android.synthetic.main.activity_create_account.*
import kotlinx.android.synthetic.main.progress_layout.*


class CreateAccountActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_account)
        init()
    }

    private fun init() {
        setSupportActionBar(createAccountToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        auth = FirebaseAuth.getInstance()
        signUpEmailEditText.isEmailValid()
        signUpNextButton.setOnClickListener(this)
        signUpFullNameEditText.addTextChangedListener(textWatcher)
        signUpEmailEditText.addTextChangedListener(textWatcher)
        signUpPasswordEditText.addTextChangedListener(textWatcher)
        signUpConfirmPasswordEditText.addTextChangedListener(textWatcher)
    }

    override fun onClick(v: View?) {
        if (!validateForm()) {
            return
        }
        createAccount(signUpEmailEditText.text.toString(), signUpConfirmPasswordEditText.text.toString())
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == android.R.id.home) {
            super.onBackPressed()
        }
        return true
    }

    private fun createAccount(email: String, password: String) {
        progressLayout.visibility = View.VISIBLE

        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val user = auth.currentUser

                    val profileUpdates = UserProfileChangeRequest.Builder()
                        .setPhotoUri(Uri.parse("https://www.qvphysiotherapy.com/wp-content/uploads/2018/12/profile-placeholder.png"))
                        .setDisplayName(signUpFullNameEditText.text.toString())
                        .build()

                    user!!.updateProfile(profileUpdates)
                    updateUI(user)
                } else {
                    Snackbar.make(createAccountContainer, "Authentication Failed.", Snackbar.LENGTH_SHORT).show()
                    updateUI(null)
                }
            }
    }

    private fun updateUI(user: FirebaseUser?) {
        progressLayout.visibility = View.GONE
        if (user != null) {
            val intent = Intent(this, UploadPhotoActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }
    }

    private fun validateForm(): Boolean {
        var valid = true
        val email = signUpEmailEditText.text.toString()

        if(!Tools.emailValid(email)) {
            signUpEmailInputLayout.error = "Invalid email address."
            valid = false
        } else {
            signUpEmailInputLayout.error = null
        }

        val password = signUpPasswordEditText.text.toString()
        if(password.length < 7){
            signUpPasswordInputLayout.error = "7 Characters Minimum"
            valid = false
        } else {
            signUpPasswordInputLayout.error = null
        }

        val confirmPassword = signUpConfirmPasswordEditText.text.toString()
        if (!TextUtils.equals(password, confirmPassword)) {
            signUpConfirmPasswordInputLayout.error = "Doesn't match."
           valid = false
        } else {
            signUpConfirmPasswordInputLayout.error = null
        }

        return valid
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {}

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            val fullName = signUpFullNameEditText.text.toString().trim()
            val email = signUpEmailEditText.text.toString().trim()
            val password = signUpPasswordEditText.text.toString().trim()
            val confirmPassword = signUpConfirmPasswordEditText.text.toString().trim()

            if(fullName.isNotEmpty() && email.isNotEmpty() && password.isNotEmpty() && confirmPassword.isNotEmpty()){
                signUpNextButton.isEnabled = true
                signUpNextButton.setBackgroundResource(R.drawable.create_account_button)
            } else {
                signUpNextButton.isEnabled = false
                signUpNextButton.setBackgroundResource(R.drawable.disable_button_color)
            }
        }
    }
}