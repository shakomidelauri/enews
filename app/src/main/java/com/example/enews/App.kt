package com.example.enews

import android.app.Application
import android.content.Context

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        context = applicationContext

    }

    companion object {
        lateinit var instance: App
        private lateinit var context: Context
        init {
            System.loadLibrary("native-lib")
        }
    }

    fun getContext() = context

    fun getApiKeyFromJNI() = stringFromJNI()

    private external fun stringFromJNI(): String
}
