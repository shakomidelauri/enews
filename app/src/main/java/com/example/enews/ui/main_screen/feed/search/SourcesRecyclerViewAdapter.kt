package com.example.enews.ui.main_screen.feed.search

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.CategoryOnClick
import kotlinx.android.synthetic.main.single_source_layout.view.*
import java.net.URL
import java.util.*

class SourcesRecyclerViewAdapter(private val sourcesList: MutableList<SourceModel?>, private val categoryOnClick: CategoryOnClick): RecyclerView.Adapter<SourcesRecyclerViewAdapter.ViewHolder>(), Filterable {

    private var sourcesListFull = mutableListOf<SourceModel?>()

    init {
        sourcesListFull.addAll(sourcesList)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        fun onBind() {
            d("sourcename",  sourcesList[adapterPosition]?.name.toString())
            itemView.singleSourceTextView.text = sourcesList[adapterPosition]!!.name
            Glide.with(itemView.context).load("http://logo.clearbit.com/${URL(sourcesList[adapterPosition]!!.url).host}").into(itemView.singleSourceImageView)

            itemView.singleSourceImageView.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            categoryOnClick.onClick(v, adapterPosition)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.single_source_layout, parent, false))
    }

    override fun getItemCount() = sourcesList.size

    private var sourceFilter= object : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val filteredSourceList = mutableListOf<SourceModel?>()

            if(constraint == null || constraint.isEmpty()){
                filteredSourceList.addAll(sourcesListFull)
            } else {
                val keyword = constraint.toString().toLowerCase(Locale.ROOT).trim()

                for(item in sourcesListFull){
                    if(item!!.name.toLowerCase(Locale.ROOT).contains(keyword)){
                        filteredSourceList.add(item)
                    }
                }
            }

            val results : FilterResults = FilterResults()
            results.values = filteredSourceList
            return results


        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            sourcesList.clear()
            sourcesList.addAll(results!!.values as MutableList<SourceModel>)
            notifyDataSetChanged()
        }

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getFilter(): Filter {
        return sourceFilter
    }
}