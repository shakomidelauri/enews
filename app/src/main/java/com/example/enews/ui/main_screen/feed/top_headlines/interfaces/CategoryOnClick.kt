package com.example.enews.ui.main_screen.feed.top_headlines.interfaces

import android.view.View

interface CategoryOnClick {
    fun onClick(view: View?, position: Int)
}