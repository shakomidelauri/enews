package com.example.enews

import org.ocpsoft.prettytime.PrettyTime
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object Tools {
    fun emailValid(emailStr: String) = android.util.Patterns.EMAIL_ADDRESS.matcher(emailStr).matches()

    fun dateToTimeFormat(receivedDateString: String): String? {
        val p = PrettyTime(Locale(getCountry()))
        var isTime: String? = null
        val sdf : SimpleDateFormat
        val date : Date?
        try {
            if (receivedDateString.length < 22){
                 sdf = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss'Z'",
                    Locale.ENGLISH
                )
                date = sdf.parse(receivedDateString)
                isTime = p.format(date)
            } else {
                sdf = SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss.SSSSSSS'Z'",
                    Locale.ENGLISH)
                date = sdf.parse(receivedDateString)
                isTime = p.format(date)
            }
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return isTime
    }

    private fun getCountry(): String {
        val locale = Locale.getDefault()
        val country = java.lang.String.valueOf(locale.country)
        return country.toLowerCase(Locale.ROOT)
    }

}