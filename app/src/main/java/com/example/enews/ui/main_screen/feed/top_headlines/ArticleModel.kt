package com.example.enews.ui.main_screen.feed.top_headlines

import android.os.Parcel
import android.os.Parcelable

class ArticleModel() : Parcelable {

        class Source() : Parcelable {
            var id = ""
            var name = ""

                constructor(parcel: Parcel) : this() {
                        id = parcel.readString()!!
                        name = parcel.readString()!!
                }

                override fun writeToParcel(parcel: Parcel, flags: Int) {
                        parcel.writeString(id)
                        parcel.writeString(name)
                }

                override fun describeContents(): Int {
                        return 0
                }

                companion object CREATOR : Parcelable.Creator<Source> {
                        override fun createFromParcel(parcel: Parcel): Source {
                                return Source(
                                    parcel
                                )
                        }

                        override fun newArray(size: Int): Array<Source?> {
                                return arrayOfNulls(size)
                        }
                }
        }

        var author = ""
        var title = ""
        var description = ""
        var url = ""
        var urlToImage = ""
        var publishedAt = ""
        var content = ""

        constructor(parcel: Parcel) : this() {
                author = parcel.readString()!!
                title = parcel.readString()!!
                description = parcel.readString()!!
                url = parcel.readString()!!
                urlToImage = parcel.readString()!!
                publishedAt = parcel.readString()!!
                content = parcel.readString()!!
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(author)
                parcel.writeString(title)
                parcel.writeString(description)
                parcel.writeString(url)
                parcel.writeString(urlToImage)
                parcel.writeString(publishedAt)
                parcel.writeString(content)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<ArticleModel> {
                override fun createFromParcel(parcel: Parcel): ArticleModel {
                        return ArticleModel(
                            parcel
                        )
                }

                override fun newArray(size: Int): Array<ArticleModel?> {
                        return arrayOfNulls(size)
                }
        }

}