package com.example.enews.ui.main_screen.feed.profile

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.enews.R
import com.example.enews.ui.main_screen.authentication.AuthenticationActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.fragment_profile.*


class ProfileFragment : Fragment(), View.OnClickListener {

    private lateinit var auth : FirebaseAuth
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<*>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_profile, container, false)
        return view
    }

    override fun onResume() {
        super.onResume()
        auth = FirebaseAuth.getInstance()
        val currentUser = auth.currentUser

        Handler().postDelayed({
            if(currentUser != null){
                Glide.with(this).load(currentUser.photoUrl).into(profileImageView)
                nameTextView.text = currentUser.displayName
                emailTextView.text = currentUser.email
            }
        }, 2000)
        bottomSheetBehavior = BottomSheetBehavior.from<View>(bottomSheet)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        logOutButton.setOnClickListener(this)
        editProfileButton.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when(v!!.id){
            R.id.logOutButton -> {
                auth.signOut()

                val intent = Intent(activity as BottomNavigationActivity, AuthenticationActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
            }

            R.id.editProfileButton -> {
                val intent = Intent(activity as BottomNavigationActivity, EditProfileActivity::class.java)
                startActivity(intent)
                requireActivity().overridePendingTransition(R.anim.slide_up, R.anim.no_change)
            }
        }
    }
}