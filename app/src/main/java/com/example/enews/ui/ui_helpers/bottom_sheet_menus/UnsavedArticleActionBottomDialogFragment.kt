package com.example.enews.ui.ui_helpers.bottom_sheet_menus

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.enews.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.sheet_menu_popup_layout.*
import java.lang.RuntimeException


class UnsavedArticleActionBottomDialogFragment : BottomSheetDialogFragment(), View.OnClickListener {
    private var mListener: ItemClickListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.sheet_menu_popup_layout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        sheetMenuSave.setOnClickListener(this)
        sheetMenuShare.setOnClickListener(this)
        sheetMenuGoToWeb.setOnClickListener(this)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mListener = if (context is ItemClickListener) {
            context
        } else {
            throw RuntimeException(
                context.toString()
                    .toString() + " must implement ItemClickListener"
            )
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    override fun onClick(view: View) {
        mListener!!.onItemClick(view)
        dismiss()
    }

    interface ItemClickListener {
        fun onItemClick(view: View)
    }

    companion object {
        const val TAG = "UnsavedArticleActionBottomDialogFragment"
        fun newInstance(): UnsavedArticleActionBottomDialogFragment {
            return UnsavedArticleActionBottomDialogFragment()
        }
    }
}