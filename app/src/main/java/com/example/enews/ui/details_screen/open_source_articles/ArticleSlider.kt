package com.example.enews.ui.details_screen.open_source_articles

import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AlphaAnimation
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.example.enews.App
import com.example.enews.R
import com.example.enews.Tools
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.MoreButtonOnClick
import com.example.enews.ui.main_screen.feed.top_headlines.interfaces.OpenNewsOnClick
import kotlinx.android.synthetic.main.single_source_article_layout.view.*


class ArticleSlider(private val articles: MutableList<ArticleModel?>, private val viewPager2: ViewPager2, private val openNewsOnClick : OpenNewsOnClick, private val moreButtonOnClick: MoreButtonOnClick) : RecyclerView.Adapter<ArticleSlider.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.single_source_article_layout, parent, false))
    }

    override fun getItemCount(): Int {
        return articles.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun onBind() {
            itemView.tag = "View$adapterPosition"

            itemView.appearingDescription.alpha = 0.0f
            Glide.with(itemView.context).load(articles[adapterPosition]!!.urlToImage).into(itemView.imageView3)
            itemView.titleTextView3.text = articles[adapterPosition]!!.title
            itemView.dateTextView3.text = Tools.dateToTimeFormat(articles[adapterPosition]!!.publishedAt)
            itemView.appearingDescription.text = articles[adapterPosition]!!.description

            itemView.appearingDescription.animate().alpha(1.0f).setDuration(1000).startDelay = 2000

            itemView.setOnClickListener(this)
            itemView.moreButton3.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when(v) {
                itemView -> openNewsOnClick.onClick(adapterPosition)
                itemView.moreButton3 -> moreButtonOnClick.onClick(adapterPosition)
            }
        }
    }
}