package com.example.enews.ui.main_screen.splash_screen

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.transition.Fade
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import com.example.enews.R
import com.example.enews.ui.main_screen.authentication.AuthenticationActivity
import com.example.enews.ui.main_screen.authentication.UploadPhotoActivity
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : AppCompatActivity() {

    private lateinit var handler: Handler
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        val fade = Fade()
        fade.excludeTarget(android.R.id.statusBarBackground, true)
        fade.excludeTarget(android.R.id.navigationBarBackground, true)
        window.enterTransition = fade
        window.exitTransition = fade

        handler = Handler()
        auth = FirebaseAuth.getInstance()

    }

    override fun onStart() {
        super.onStart()
        handler.postDelayed(runnable, 2000)
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

    private val runnable = Runnable {
        val currentUser = auth.currentUser
        if (currentUser == null) {
            openAuthentication()
        } else {
            if (currentUser.photoUrl.toString() == "https://www.qvphysiotherapy.com/wp-content/uploads/2018/12/profile-placeholder.png")
                openUploadPhoto()
            else
                openNewsFeed()
        }
    }

    private fun openNewsFeed(){
        val intent = Intent(this, BottomNavigationActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun openUploadPhoto(){
        val intent = Intent(this, UploadPhotoActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }

    private fun openAuthentication(){
        val intent = Intent(this, AuthenticationActivity::class.java)
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this, splashIcon, ViewCompat.getTransitionName(splashIcon)!!
        )
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent, options.toBundle())
    }
}