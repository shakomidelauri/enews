package com.example.enews.ui.details_screen.open_article

import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.viewpager.widget.PagerAdapter
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import kotlinx.android.synthetic.main.open_article_webview_layout.view.*
import kotlinx.android.synthetic.main.progress_layout_circle.view.*

class OpenArticlesPagerAdapter(private val articles: MutableList<ArticleModel?>): PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = articles.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = LayoutInflater.from(container.context).inflate(R.layout.open_article_webview_layout, container, false)

        itemView.progressLayoutCircle.visibility = View.VISIBLE
        itemView.articleWebView.settings.loadsImagesAutomatically = true
        itemView.articleWebView.settings.javaScriptEnabled = true
        itemView.articleWebView.settings.domStorageEnabled = true
        itemView.articleWebView.settings.supportZoom()
        itemView.articleWebView.settings.builtInZoomControls = true
        itemView.articleWebView.settings.displayZoomControls = false
        itemView.articleWebView.settings.supportMultipleWindows()
        itemView.articleWebView.scrollBarStyle = (View.SCROLLBARS_INSIDE_OVERLAY)

        itemView.articleWebView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                itemView.progressLayoutCircle.visibility = View.GONE
            }
        }
        itemView.articleWebView.loadUrl(articles[position]!!.url)

        container.addView(itemView)
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}