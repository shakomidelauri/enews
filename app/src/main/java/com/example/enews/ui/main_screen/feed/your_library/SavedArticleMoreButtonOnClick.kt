package com.example.enews.ui.main_screen.feed.your_library

interface SavedArticleMoreButtonOnClick {
    fun onClick(position: Int)
}