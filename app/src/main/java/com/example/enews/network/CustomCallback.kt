package com.example.enews.network

interface CustomCallback {
    fun onFailure(error: String)
    fun onSuccess(body: String)
}