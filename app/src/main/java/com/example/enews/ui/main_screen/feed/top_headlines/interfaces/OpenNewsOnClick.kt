package com.example.enews.ui.main_screen.feed.top_headlines.interfaces

interface OpenNewsOnClick {
    fun onClick (position : Int)
}