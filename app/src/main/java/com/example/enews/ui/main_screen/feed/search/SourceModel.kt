package com.example.enews.ui.main_screen.feed.search

class SourceModel {
    var id = ""
    var name = ""
    var description = ""
    var url = ""
    var category = ""
    var language = ""
    var country = ""
}