package com.example.enews.ui.details_screen.open_article

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import com.example.enews.R
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticle
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSource
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.activeArticleSourceList
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.databaseSavedArticles
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.selectedPosition
import com.example.enews.ui.main_screen.feed.BottomNavigationActivity.Companion.userId
import com.example.enews.ui.main_screen.feed.top_headlines.ArticleModel
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_open_article.*
import kotlinx.android.synthetic.main.open_article_webview_layout.*


class OpenArticleActivity : AppCompatActivity() {

    private var isFinalized = false
    private var savedArticleList = mutableListOf<ArticleModel?>()
    private lateinit var openArticlesPagerAdapter : OpenArticlesPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_article)
        init()
    }

    private fun init() {
        setSupportActionBar(openArticleToolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)

        openArticlesPagerAdapter = OpenArticlesPagerAdapter(activeArticleList)
        val openArticleViewPager = findViewById<View>(R.id.openArticleViewPager) as ViewPager
        openArticleViewPager.adapter = openArticlesPagerAdapter
        val tabLayout = findViewById<View>(R.id.tabDots) as TabLayout
        tabLayout.setupWithViewPager(openArticleViewPager, true)

        databaseSavedArticles.child(userId).child("articles")
            .addChildEventListener(object : ChildEventListener {
                override fun onCancelled(error: DatabaseError) {}
                override fun onChildMoved(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildChanged(snapshot: DataSnapshot, previousChildName: String?) {}
                override fun onChildAdded(snapshot: DataSnapshot, previousChildName: String?) {
                    if (snapshot.exists()) {
                        val article = snapshot.getValue(ArticleModel::class.java)
                        savedArticleList.add(article)
                    }
                }

                override fun onChildRemoved(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {

                        var found = false
                        var removedPosition = -1

                        val article = snapshot.getValue(ArticleModel::class.java)
                        for (i in savedArticleList.indices) {
                            if (savedArticleList[i]!!.title == article!!.title) {
                                found = true
                                removedPosition = i
                            }
                        }

                        if (found) {
                            savedArticleList.removeAt(removedPosition)
                        }
                    }
                }
            })

        openArticleBackButton.setOnClickListener{
            super.onBackPressed()
        }
    }

    override fun onPostResume() {
        super.onPostResume()
        openArticleViewPager.currentItem = selectedPosition
    }

    override fun onBackPressed() {
        if (articleWebView.canGoBack()) {
            articleWebView.goBack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onPrepareOptionsMenu(menu: Menu?): Boolean {
        super.onPrepareOptionsMenu(menu)
        if (isFinalized) {
            var articleIsSaved = false
            if(activeArticleList.size != 0){
                for (article in savedArticleList) {
                    if (article!!.title == activeArticleList[openArticleViewPager.currentItem]!!.title) {
                        articleIsSaved = true
                    }
                }
            }


            if (articleIsSaved) {
                menu!!.findItem(R.id.saveArticle).isVisible = false
                menu.findItem(R.id.removeArticle).isVisible = true
            } else {
                menu!!.findItem(R.id.saveArticle).isVisible = true
                menu.findItem(R.id.removeArticle).isVisible = false
            }
            this.invalidateOptionsMenu()
        }
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.open_article_menu, menu)
        isFinalized = true
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        selectedPosition = openArticleViewPager.currentItem
        activeArticle = activeArticleList[openArticleViewPager.currentItem]
        activeArticleSource = activeArticleSourceList[openArticleViewPager.currentItem]

        when (item.itemId) {
            android.R.id.home -> super.onBackPressed()
            R.id.saveArticle -> {
                val viewPagerPosition = openArticleViewPager.currentItem

                val articleId: String? = databaseSavedArticles.push().key
                if (articleId != null) {
                    databaseSavedArticles
                        .child(userId)
                        .child("articles")
                        .child(articleId)
                        .setValue(activeArticleList[viewPagerPosition])
                    databaseSavedArticles
                        .child(userId)
                        .child("articleSources")
                        .child(articleId)
                        .setValue(activeArticleSourceList[viewPagerPosition])

                    Snackbar.make(
                        openArticleContainer,
                        "Article saved to your library",
                        Snackbar.LENGTH_SHORT
                    ).show()
                }
            }
            R.id.removeArticle -> {
                val articleQuery: Query = databaseSavedArticles
                    .child(userId)
                    .child("articles")
                    .orderByChild("title")
                    .equalTo(activeArticle!!.title)

                articleQuery.addListenerForSingleValueEvent(object : ValueEventListener {
                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        for (articleSnapshot in dataSnapshot.children) {
                            databaseSavedArticles.child(userId).child("articleSources")
                                .child(articleSnapshot.ref.key!!).removeValue()
                            articleSnapshot.ref.removeValue()

                            Snackbar.make(
                                openArticleContainer,
                                "Article removed from your library",
                                Snackbar.LENGTH_SHORT
                            ).show()
                        }
                    }

                    override fun onCancelled(databaseError: DatabaseError) {}
                })
            }
            R.id.shareArticle -> {
                val intent = Intent(Intent.ACTION_SEND)
                intent.type = "text/plain"
                val sharedText =
                    "${activeArticle?.title}\n\n${activeArticle?.description}\n\n${activeArticle?.url}"
                intent.putExtra(Intent.EXTRA_TEXT, sharedText)
                startActivity(Intent.createChooser(intent, "Share news using:"))
            }
            R.id.goToArticleWebsite -> {
                val browserIntent = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(activeArticle!!.url)
                )
                startActivity(browserIntent)
            }
        }
        return false
    }
}