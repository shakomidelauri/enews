#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jstring JNICALL
Java_com_example_enews_App_stringFromJNI(JNIEnv *env, jobject/* this */) {
    std::string apiKey = "c3e8e6f9f06048d8942a32bd8a13b3ba";
    return env->NewStringUTF(apiKey.c_str());
}